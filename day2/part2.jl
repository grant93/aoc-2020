
function process(lines)
    total = 0
    while lines != []
        range, value, str  = split(pop!(lines))
        lower, upper = split(range, "-")
        # gross casting because i have no idea about this language
        if (value[1] == str[Integer(parse(Float64, lower))]) != (value[1] == str[Integer(parse(Float64, upper))])
            total += 1
        end
    end
    total
end

f = open("input.txt")
lines = readlines(f)
print(process(lines))

