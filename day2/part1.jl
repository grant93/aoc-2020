
function process(lines)
    total = 0
    while lines != []
        range, target, str = split(pop!(lines))
        occurences = count(c -> c == target[1], collect(str))
        lower, upper = split(range, "-")
        if parse(Float64, String(upper)) >= occurences && occurences >= parse(Float64, String(lower))
            total += 1
        end
    end
    total
end

f = open("input.txt")
lines = readlines(f)
print(process(lines))

