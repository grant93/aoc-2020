
def bin_to_int(data):
    new = ''
    for i in data:
        new += ('0' if i in ['F', 'L'] else '1')
    return int(new, 2)


def calculate_seat_ids(data):
    seat_ids = []
    for _pass in data:
        seat_ids.append((bin_to_int(_pass[:7]) * 8) + bin_to_int(_pass[-3:]))
    return seat_ids


def part1(data):
    return max(calculate_seat_ids(data))


def part2(data):
    passes = calculate_seat_ids(data)
    passes.sort()
    for i in range(0, len(passes)):
        if passes[i] + 1 != passes[i+1]:
            return passes[i] + 1


with open('input.txt') as f:
    a = f.readlines()
    passes = [i.strip() for i in a] 
    print(part1(passes))
    print(part2(passes))

