#include <stdio.h>
#include <stdlib.h>

#define MAX_LINE 256

struct gradient {
    int dx;
    int dy;
};


int count_lines(char *filename, int *line_length) {
    FILE *fp;
    char c;
    int count = 0;
    int length_count = 0;
    int length_flag = 0;

    fp = fopen(filename, "r");
    if (!fp) {
        return 0;
    }

    while ((c = getc(fp)) && c != EOF) {
        if (c == '\n') {
            count++;

	    if (!length_flag) {
                *line_length = length_count;
		length_flag = 1;
	    }
        }
	length_count ++;
    }

    fclose(fp);
    return count;
}

/* allocate a 2d array for our map */
int init_map(char ***map, char *filename, int *line_count) {
    FILE *fp;
    size_t line_size = MAX_LINE;
    int tmp = 0;
    int line_length = 0;
    char **array = NULL;


    tmp = count_lines(filename, &line_length);
    if (!tmp) {
        return 0;
    }
    *line_count = tmp;

    fp = fopen(filename, "r");
    if (!fp) {
        return 0;
    }

    array = malloc(*line_count * sizeof(*array));
    if (!(*array)) {
        return 0;
    }

    for(int i = 0; i < *line_count; i++) {
	array[i] = calloc(MAX_LINE, sizeof(*array));
        if (-1 == getline(&(array[i]), &line_size, fp)) {
	    return 0;
	}
    }

    fclose(fp);
    *map = array;
    return line_length;
    
}

/* emulate the map ongoing on the x axis by using a mod operation */
int sled_run(struct gradient grad, char **map, int line_count, int line_length) {
    int x = 0;
    int y = 0;
    int tree_count = 0;

    while (y < line_count) {
        if ((map[y][x % line_length]) == '#') {
	    tree_count += 1;
	}
        x += grad.dx;
        y += grad.dy;
    }
    return tree_count;
}

/* dunno why i bothered trying.. */
int clean_up(char **map, int line_count) {
    for(int i = 0; i < line_count; i++) {
        free(map[i]);
	map[i] = NULL;
    }
    free(map);
    map = NULL;
}


int part_one(char **map, int line_count, int line_length) {
    struct gradient grad = {3, 1};
    int result = sled_run(grad, map, line_count, line_length);
    printf("part one solution is: %d\n", result);
    return result;
}


int part_two(char **map, int line_count, int line_length) {
    struct gradient grad[] = { {1, 1}, {3,1}, {5,1}, {7,1}, {1,2} };
    unsigned int result = 1;

    unsigned long num_gradients = sizeof(grad) / sizeof(struct gradient);

    for(int i=0; i < num_gradients; i++) {
        result *= sled_run(grad[i], map, line_count, line_length);
    }
    printf("part two solution is: %u\n", result);
    return result;
}


int main(int argc, char **argv) {
    int line_count = 0;
    int line_length = 0;
    char ** map = {NULL};

    if (!(line_length = init_map(&map, "input.txt", &line_count))) {
        return 0;
    }

    part_one(map, line_count, line_length);
    part_two(map, line_count, line_length);
    clean_up(map, line_count);
    return 1;

}

