import System.IO
import Data.List

-- haskell doesn't handle 3 member tuples...
fst3 :: (a,b,c) -> a
fst3 (x,_,_) = x

snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x

thrd3 :: (a,b,c) -> c
thrd3 (_,_,x) = x

-- threesum algorithm
walkList :: [Int] -> Int -> Int -> (Int, Int, Int)
walkList nums extra target
    | nums == [] = (0, 0, 0)
    | (head nums) + (last nums) + extra == target = (head nums, last nums, extra)
    | (head nums) + (last nums) + extra > target = walkList (init nums) extra target
    | (head nums) + (last nums) + extra < target = walkList (tail nums) extra target

-- base function call now maintains it's own pointer to walk down the list
threeSum :: [Int] -> Int-> (Int, Int, Int)
threeSum nums target
    | nums == [] = (0, 0, 0)
    | a == (0, 0, 0) = threeSum (tail nums) target
    | otherwise = walkList (tail (sort nums)) (head nums) target 
    where
        a = walkList (tail (sort nums)) (head nums) target

main = do 
    file <- readFile "input.txt"
    let a = (threeSum (map (read::String->Int) (lines file)) 2020)
    print ((fst3 a) * (snd3 a) * (thrd3 a))
