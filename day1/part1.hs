import System.IO
import Data.List

-- twosum algorithm
walkList :: [Int] -> Int -> (Int, Int)
walkList nums target
    | nums == [] = (0, 0)
    | (head nums) + (last nums) == target = (head nums, last nums)
    | (head nums) + (last nums) > target = walkList (init nums) target
    | (head nums) + (last nums) < target = walkList (tail nums) target

twoSum :: [Int] -> Int-> (Int, Int)
twoSum nums target = walkList (sort nums) target
    

main = do 
    file <- readFile "input.txt"
    let a = (twoSum (map (read::String->Int) (lines file)) 2020)
    print ((fst a) * (snd a))
