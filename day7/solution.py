
def to_graph(data):
    rules = {}
    for line in data:
        split_strings = line.split(' ')
        base_colour = ''.join(split_strings[:2])

        i = 4 
        contains = []
        while i < len(split_strings):
            test  = ''.join(split_strings[i+1:i+3])
            if split_strings[i] != 'no':
                contains.append((test, int(split_strings[i])))
            i += 4
        rules[base_colour] = contains 
    return rules


def find_path(graph, start, end, path=[]):
    path = path + [start]
    if start == end:
        return path
    if start not in graph.keys():
        return None
    for node, _ in graph[start]:
        if node not in path:
            tmp = find_path(graph, node, end, path)
            if tmp:
                return tmp
    return None


def total_baggy_bois(name, rules):
    count = 0
    for bag_name, n in rules[name]:
        num = 1 + total_baggy_bois(bag_name, rules)
        count += num * n
    return count


def part1(data):
    count = 0
    rules = to_graph(data)
    for i in rules.keys():
        b = find_path(rules, i, "shinygold")
        # don't include the shinygold node
        if b and b[0] != "shinygold":
            count += 1
    return count

def part2(data):
    rules = to_graph(data)
    return total_baggy_bois("shinygold", rules)


with open('input.txt') as f:
    a = f.readlines()
    rules = [i.strip() for i in a] 
    print(part1(rules))
    print(part2(rules))
