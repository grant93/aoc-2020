def part1(data):
    count = 0
    for group in data:
        count += len(set.union(*map(set, group)))
    return count
            
def part2(data):
    count = 0
    for group in data:
        group = [a.strip() for a in group]
        count += len(set.intersection(*map(set, group)))
    return count


with open('input.txt') as f:
    lines = f.readlines()
    templist = []
    customs_forms = []
    for line in lines:
        line = line.strip()
        if not line:
            customs_forms.append(templist)
            templist = []
        else:
            templist.append(line.strip())
    customs_forms.append(templist)
    print(part1(customs_forms))
    print(part2(customs_forms))
