class virtual_machine():
    def __init__(self, instruction_set):
        self.accumulator = 0
        self.position = 0
        self.instruction_set = instruction_set
        self.order = []
    def accumulate(self, value):
        self.accumulator += int(value) 
        self.position += 1
    def jump(self, value):
        self.position += int(value)
    def noop(self, value):
        self.position += 1
        return
    def run(self):
        status = True
        while self.position < len(self.instruction_set):
            instruction, value = self.instruction_set[self.position]
            if self.position in self.order:
                status = False
                break
            self.order.append(self.position)
            if instruction == 'nop':
                self.noop(value)
            elif instruction == 'acc':
                self.accumulate(value)
            elif instruction == 'jmp':
                self.jump(value)
            else:
                print("shit is fucked up")
                return 0 
        return status, self.accumulator

def part1(data):
    _, a = virtual_machine(instructions).run()
    print(a)

def part2(data):
    for i in range(len(instructions)):
        instruction, value = instructions[i]
        if instruction == 'nop':
            instructions[i] = 'jmp', value
        elif instruction == 'jmp':
            instructions[i] = 'nop', value
        status, accum = virtual_machine(instructions).run()
        if status:
            print(accum)
            break
        else:
            instructions[i] = instruction, value

with open('input.txt') as f:
    a = f.readlines()
    lines = [i.strip() for i in a] 
    instructions = []
    for i in lines:
        instructions.append(tuple(i.split(' ')))
    part1(instructions)
    part2(instructions)

